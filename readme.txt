tngintegrate.module readme.txt
by Arturo Ramos
arturo.ramos2@gmail.com
February 19, 2007


This is a beta version of a module that integrates Darrin Lythgoe's The Next Generation (TNG) genealogy
software with Drupal.

The module basically creates a new account for any Drupal user who does not exist in the TNG user tables.

For users that do exist, the module creates an autosubmit form that logs the user automatically into TNG.

This module will only work if TNG and Drupal share the same database.

To install, simply put the module in the /sites/all/modules folder and enable the module through the
admin/build/modules menu.

You will then need to configure the module by setting several key variables in the /admin/settings menu under
TNG Genealogy.  These must be set.  Even if all of the values appear correct, click on "Save Configuration" to
set the variables.

1. Path to TNG Directory.  This should point to the folder on the server where the TNG software resides.
   The default is "tng/" which means that TNG would be installed in a folder within the folder where the
   Drupal installation resides.  Make sure to add the trailing slash to the folder name.
   
2. Error logging.  This determines whether the errors get logged into Drupal's watchdog module or get displayed
   on the screen.  The default is to use the Watchdog module.
  
3. Admin e-mail address for TNG.  This is the "from:" address for welcome messages when new users are created.

4. Subject Line for TNG Welcome e-mail message - self explanatory

5. Text of TNG welcome e-mail message - self explanatory.  This could include certain instructions on how to gain
   more than access privileges, for example, having a tree created for which the user would have edit privileges.

6. If you are using a TNG version 6.0 or above, you will need to comment out line 101 and decomment line 103 so
   that the module uses the password encryption that corresponds with the respective version of TNG.
   
In addition, you will have to grant users access to the module (to allow them to log in automatically from Drupal
and for Drupal to create new accounts for users) through the admin/user/access menu.  There is one permission
that needs to be set for each Drupal user group "access TNG".

Finally, you will need to add a menu for the module, which will allow users to click and log onto TNG.  This can
be done through the admin/build/menu menu.  You can add a primary link that points to "tng" or add the suggested
menu item to the navigation menu under "TNG Genealogy."

There are certain shortcomings with the current module which will be addressed in future versions, namely:

1. Currently Drupal username is used for "Description", "Real Name" and "Username" in TNG when a new user is
   created.  Future version will allow admin to optionally choose which Drupal profile fields to use for these
   fields in TNG.
   
2. Currently link between Drupal user table and TNG user table is by username.  This presents problems if a user
   changes his/her username in Drupal.  Future version will base link on user ids with a correspondence table.

3. Assigning additional privileges such as upload and edit privileges in TNG must be done through TNG and cannot
   be set through Drupal.  Future version will allow admin option to create and assign edit privileges to trees
   with account creation or through the user module in Drupal.
   


